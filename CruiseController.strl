module CruiseController:
% Global Signals
input On; % Enable cruise controller
input Off; % Disable cruise controller
input Resume; % Resume cruise controller
input Set; % Set the current speed as the target speed
input QuickDecel; % Decrease cruise speed by 2.5
input QuickAccel; % Increase cruise speed by 2.5

sensor Accel: float;
sensor Brake: float;
sensor Speed: float;

output CruiseSpeed: float;
output ThrottleCmd: float;
output CruiseState: integer; % OFF = 1, ON = 2, STDBY = 3, DISABLE = 4

loop % main loop
	abort
		every tick do
			emit CruiseState(1);
			emit ThrottleCmd(?Accel);
		end every;
	when On;
	abort
		run Runner / CruiseControllerRunner[
			signal 	Resume / Resume,
					Set / Set,
					QuickDecel / QuickDecel,
					QuickAccel / QuickAccel,
					CruiseSpeed / CruiseSpeed,
					ThrottleCmd / ThrottleCmd,
					CruiseState / CruiseState
		];
	when Off;
end loop % end main loop

end module


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module CruiseControllerRunner:

function regulateThrottle(integer, float, float): float;

input Resume; % Resume cruise controller
input Set; % Set the current speed as the target speed
input QuickDecel; % Decrease cruise speed by 2.5
input QuickAccel; % Increase cruise speed by 2.5

sensor Accel: float;
sensor Brake: float;
sensor Speed: float;

output CruiseSpeed: float;
output ThrottleCmd: float;
output CruiseState: integer; % OFF = 1, ON = 2, STDBY = 3, DISABLE = 4

constant SpeedMin = 30.0f: float;
constant SpeedMax = 150.0f: float;
constant SpeedInc = 2.5f: float;
constant PedalsMin = 3.0f: float;

var state := 2, turnedOn := 1: integer, setSpeed := ?Speed : float in

if setSpeed < SpeedMin then
	setSpeed := SpeedMin;
end if;
if setSpeed > SpeedMax then
	setSpeed := SpeedMax;
end if;

% Emit the cruise state at 1 (OFF) before the system finishes the first pause
emit CruiseState(1);

signal SpeedSafe, SpeedUnsafe in
	loop % main loop
		pause;
		trap T1 in

			% Immediate transition to STDBY state
			% When Brake is pressed
			if ?Brake >= PedalsMin then
				state := 3;
			end if;

			% Update CruiseSpeed
			present QuickDecel then
				setSpeed := setSpeed - SpeedInc;
			end present;
			present QuickAccel then
				setSpeed := setSpeed + SpeedInc;
			end present;

			% Set the speed if required
			present Set then
				setSpeed := ?Speed;
			end present;

			% Bound the speed
			if setSpeed < SpeedMin then
				setSpeed := SpeedMin;
			end if;
			if setSpeed > SpeedMax then
				setSpeed := SpeedMax;
			end if;

			emit CruiseState(state);
			emit CruiseSpeed(setSpeed);

			% Check speed safety
			if SpeedMin <= ?Speed and ?Speed <= SpeedMax then
				emit SpeedSafe;
			else
				emit SpeedUnsafe;
			end if;

			% ON
			if state = 2 then
				% Throttle
				emit ThrottleCmd(
					regulateThrottle(turnedOn, setSpeed, ?Speed)
				);
				turnedOn := 0;

				% If we accelerate then goto DISABLE
				if ?Accel >= PedalsMin then
					state := 4;
				end if;
				% If SpeedUnsafe then goto DISABLE
				present SpeedUnsafe then
					state := 4;
				end present;

				exit T1;
			end if;

			% STDBY
			if state = 3 then
				present Resume then
					% Resuming CruiseControl

					% Check if we go to ON
					present SpeedSafe then
						if ?Accel <= PedalsMin then
							state := 2;
							turnedOn := 1;
						else
							state := 4;
						end if;
					else
						state := 4;
					end present;
				end present;
				exit T1;
			end if;

			% DISABLE
			if state = 4 then
				% Check if our current speed is safe
				present SpeedSafe then
					% If we are not accelerating as well...
					% ... we can go back go ON
					if ?Accel < PedalsMin then
						state := 2;
						turnedOn := 1;
					end if;
				end present;

				exit T1;
			end if;

		end trap; % T1
	end loop; % end main loop
end signal
end var

end module
